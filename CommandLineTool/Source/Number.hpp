//
//  Number.hpp
//  CommandLineTool
//
//  Created by Callum McNeill on 23/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#ifndef Number_hpp
#define Number_hpp

#include <stdio.h>

template <class Type>
class Number
{
public:
    Type get() const
    {
        std::cout << "yes value: ";
        return value;
    }
    void set(Type newValue)
    {
        value = newValue;
    }
private:
    Type value;
};

#endif /* Number_hpp */
