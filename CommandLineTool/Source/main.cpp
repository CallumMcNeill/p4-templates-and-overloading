//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "Number.hpp"

//Main
int main()
{
    Number<float> numf;
    Number<int> numi;
    Number<std::string> numstr;
    
    numf.set (0.5f);
    numi.set (2);
    numstr.set ("yess");
    
    std::cout << numf.get() << "\n";
    std::cout << numi.get() << "\n";
    std::cout << numstr.get() << "\n";

    return 0;
}